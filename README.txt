Overview:
--------
The book review module allows a site to publish book reviews.  A book review
is simply a glorified node, providing custom fields that are appropriate
to writing a book review.


Features:
--------
 - Provides fields for booktitle, publisher, copyright, isbn, # of pages, price,
   synopsis, table of contents and the review text.
 - Supports unlimited number of authors.
 - Supports unlimited number of web links.
 - Provides option block allowing a link to an external bookstore, such as
   Amazon


Installation and configuration:
------------------------------
Please refer to the INSTALL file for complete installation and 
configuration instructions.


Requires:
--------
 - Drupal 4.7


Credits:
-------
 - Written by Jeremy Andrews <jeremy@kerneltrap.org>
